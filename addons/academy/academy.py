##############################################################################
#
# Copyright (c) 2004 TINY SPRL. (http://tiny.be) All Rights Reserved.
#                    Fabien Pinckaers <fp@tiny.Be>
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from osv import osv, fields

class academy_course_category(osv.osv):
   
    def name_get(self,cr,uid,ids,context={}):
        if not len(ids):
            return []
        reads = self.read(cr, uid, ids, ['name','parent_id'], context)
        res = []
        for record in reads:
            name = record['name']
            if record['parent_id']:
                name = record['parent_id'][1]+' / '+name
            res.append((record['id'], name)) 
        return res
     
    def _name_get_fnc(self, cr, uid, ids, prop, nom_camp, context):
        res = self.name_get(cr, uid, ids, context)
        return dict(res)

    _name = 'academy.course.category'
    _columns = {
        'name': fields.char('Category',size=16),
        'parent_id': fields.many2one('academy.course.category','Parent category'),
        'course_ids': fields.one2many('academy.course','category_id','Groups'),
        'complete_name': fields.function(_name_get_fnc, method=True, string="Complete name", type="char", readonly='True'),
    }
    
    def _check_recursion(self, cr, uid, ids): 
        level = 100
        while len(ids):
            cr.execute('select distinct parent_id from academy_course_category where id in %s', (tuple(ids),))
            ids = filter(None, map(lambda x:x[0], cr.fetchall()))
            if not level:
                return False 
            level -= 1 
        return True
    
    _constraints = [(_check_recursion, 'Error! You can not create recursive categories.', ['parent_id'])]
academy_course_category()

class academy_classroom(osv.osv):
    _name = 'academy.classroom'
    _columns = {
        'name': fields.char('Classroom',size=35),
        'address_id': fields.many2one('res.partner.address','Address'),
        'capacity': fields.integer('Capacity'),
        'state': fields.selection([('usable','Usable'),('in works','In works')],'State'),
        'active': fields.boolean('Active'),
        'ocupada': fields.boolean('Ocupada'),
    }
    
    _defaults = {
                 'ocupada': False,
                 }
    '''
    # Versio amb read
    def name_get(self,cr,uid,ids,context={}):
        if not len(ids):
            return []
        reads = self.read(cr, uid, ids, ['name','capacity'], context)
        res = []
        for record in reads:
            name = record['name'] + " (" + str(record['capacity']) + ")"
            res.append((record['id'], name)) 
        return res
    
    '''
    
    # Amb browse
    def name_get(self,cr,uid,ids,context={}):
        if not len(ids):
            return []
        reads = self.browse(cr, uid, ids, context)
        res = []
        for i in reads:
            name = i.name + " (" + str(i.capacity) + ")"
            res.append((i.id, name)) 
        return res
    '''
    def unlink(self, cr, uid, ids, context=None):
        curs = self.pool.get('academy.course')
        cerca = curs.search(cr,uid,[('classroom_id','=',ids[0])],count=True)
        if cerca == 0:
            print "Es pot esborrar, no te cursos assignats!"
        else:
            print "No es pot esborrar, te", cerca,  "curs/cursos assignat/s"
        return True
    '''
    def unlink(self, cr, uid, ids, context=None):
        curs = self.pool.get('academy.course')
        aEsborrar = []
        noEsborrar = []
        for i in ids:
            cerca = curs.search(cr,uid,[('classroom_id','=',i)],count=True)
            if cerca == 0:
                aEsborrar.append(i)
            else:
                noEsborrar.append(i)
        retorn = False        
        if len(aEsborrar):
            #print "Eliminem els elements: ", aEsborrar
            retorn = osv.osv.unlink(self, cr, uid, aEsborrar, context=context)
        contador = len(noEsborrar)
        if contador:
            raise osv.except_osv("Error", "Hi ha " + str(contador) + " elements que no s'han esborrat. Consulta el log.")
        return retorn

    
    def canviCapacity(self,cr,uid,ids, capacity, active):
        print capacity, " ", active
        if capacity > 100:
            return {'value': {'active':False, 'capacity': 100}}
        return {}    
       
academy_classroom()

class academy_course(osv.osv):
    _name = 'academy.course'
    _columns = {
        'name': fields.char('Course',size=32),
        'category_id': fields.many2one('academy.course.category','Category'),
        'classroom_id': fields.many2one('academy.classroom','Classroom'),
        'difficulty': fields.selection([('low','Low'),('medium','Medium'),('high','High')],'Difficulty'),
        'hours': fields.float('Studies Hours'),
        'state': fields.selection([('draft','Draft'),('testing','Testing'),('stable','Stable')],'State'),
        'description': fields.text('Description'),
    }
    _defaults = {
        'difficulty':'high',
        'hours': 99,
        'state': 'stable', 
    }
    
    def canviDifficulty(self,cr,uid,ids,difficulty,hours):
        if difficulty == 'low' and hours > 50:
            return {'value':{'hours': 50}}
        return {}
        
        
    def _check_description(self,cr,uid,ids):
        for course in self.browse(cr,uid,ids):
            if not course.description:
                return False
            elif len(course.description) < 50:
                return False
            else:
                return True
    
    def _check_hours(self,cr,uid,ids):
        for course in self.browse(cr,uid,ids):
            if course.hours >= 50 and course.difficulty == 'low':
                return False
            return True
    _constraints = [#(_check_description,"Error, the description must be 50 characters at least!",['description']),
                    (_check_hours,"A course with more than 50 hours cannot be low!",['hours','difficulty']),
                    ]

    def create(self, cr, uid, vals, context=None):
        # Instanciem la classe "classroom" per poder accedir als seus metodes
        class_classe = self.pool.get('academy.classroom')
        # Obtenim la instancia de l'objecte classe que ha seleccionat l'usuari
        obj_classe = class_classe.browse(cr, uid, vals['classroom_id'], context)
        # Si la classe te l'atribut ocupada a True
        if obj_classe.ocupada:
            # Excepcio
            raise osv.except_osv("Error", "La classe esta ocupada")
        else:
            # Escrivim a la classe que ha seleccionat l'usuari ocupada a True
            class_classe.write(cr,uid,vals['classroom_id'],{'ocupada': True})
            # Creem l'objecte i retornem el seu identificador
            return osv.osv.create(self, cr, uid, vals, context)
 
 
    def unlink(self, cr, uid, ids, context=None):
        # Instanciem la classe "classroom" per poder accedir als seus metodes
        classes = self.pool.get('academy.classroom')
        # Per a cada curs
        for record in self.browse(cr, uid, ids):
            # Escrivim a la classe que pertany al curs, ocupada a False
            classes.write(cr, uid, record.classroom_id.id , {'ocupada': False})
        # Retornem True o False segons el calcul             
        return osv.osv.unlink(self, cr, uid, ids, context=context)

academy_course()
    
    



