{
        "name" : "academy",
        "version" : "0.1",
        "author" : "2DAM",
        "website" : "http://copernic.cat",
        "category" : "Unknown",
        "description": """ Primer modul  """,
        "depends" : ['base'],
        "init_xml" : [ ],
        "demo_xml" : [ ],
        "update_xml" : ['views/academy_view.xml','views/academy_menu.xml', 
			'wizard/wizard_academy.xml'
			],
        "installable": True
}
