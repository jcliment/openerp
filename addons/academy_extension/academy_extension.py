##############################################################################
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
class academy_course(osv.osv):
    _name = 'academy.course'
    _inherit = 'academy.course'
    #Do not touch _name it must be same as _inherit
    #_name = 'academy.course'
    
    _columns = {
        'student_ids': fields.many2many('res.partner.address', 'course_address_rel', 'course_id', 'res_partner_address_id', 'Students', help='Students enrolled in the course'),
        'remaining_seats': fields.integer('Remaining seats', help='Remaining seats of the course'),
        'complete': fields.boolean('Complete', help='Complete course. No more one can be enroled.'),
    }

    def onchange_classroom(self, cr, uid, ids, classroomid, context=False):
        print classroomid 
        if classroomid:
            class_object = self.pool.get('academy.classroom')
            aula = class_object.browse(cr, uid, classroomid)
            capacity = aula.capacity
            return {'value': {'remaining_seats': capacity}}
        return {'value': {}}
     
academy_course()

